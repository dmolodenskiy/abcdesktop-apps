## To add applications to bard-external.embl.de

curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @cellprofiler/cellprofiler.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @cellprofiler_cellpose/cp_cp.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @ chimera/chimera.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @chimera/chimera.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @chrome/chrome.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @ fiji/fiji.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @gedit/gedit.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @ilastik/ilastik.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @napari/napari.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @nautilus/nautilus.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @nextflow/nextflow.json
curl -X PUT -H 'Content-Type: text/javascript' https://bard-external.embl.de/API/manager/image -d @rstudio/rstudio.json
